<?php
require_once 'vendor/autoload.php';

use R3C\Wordpress\Doctrine\DoctrineFactory;

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet(DoctrineFactory::entityManager());