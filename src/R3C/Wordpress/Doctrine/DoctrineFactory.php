<?php
namespace R3C\Wordpress\Doctrine;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\EventManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Cache\Cache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\CachedReader;

use \Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter;

class DoctrineFactory
{
    public static $entityManager;
    public static $entitiesPath;
    public static $eventManager;

    //O EntityManager precisa ser recriado?
    public static $recreate = false;

    public static function addEntityPath($entitiesNamespace, $path)
    {
        if (self::$entitiesPath == null) {
            //BaseEntity precisa existir
            self::$entitiesPath = [
                'R3C\Wordpress\Doctrine\Entities' => __DIR__ . '/Entities'
            ];
        }

        //Recriar o EntityManager caso ele já exista
        if (self::$entityManager != null) {
            self::$recreate = true;
        }

        self::$entitiesPath[$entitiesNamespace] = $path;
    }

    public static function entityManager()
    {
        if (isset(self::$entityManager) && !self::$recreate) {
            return self::$entityManager;
        }

        //Inicializa as variáveis do Wordpress (caso ainda não tenham sido inicializadas)

        if ( !defined('ABSPATH') ) {
            $wordpressFolder = self::wordpressConfigFolder();
            define('ABSPATH', $wordpressFolder . '/wp/');
            include_once ABSPATH . '../wp-config.php';
        }

        self::$recreate = false;

        return self::createEntityManager();
    }

    public static function createEntityManager()
    {
        $paths = self::$entitiesPath;

        $isDevMode = true;

        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);

        if ($isDevMode) {
            $defaultDriver = new AnnotationDriver(
                new CachedReader(
                    new AnnotationReader(),
                    new ArrayCache()
                ),
                $paths
            );
        } else {
            $defaultDriver = $config->getMetadataDriverImpl();
        }

        $driverChain = new \Doctrine\ORM\Mapping\Driver\DriverChain();

        \Gedmo\DoctrineExtensions::registerAbstractMappingIntoDriverChainORM(
            $driverChain
        );

        foreach ($paths as $key => $value) {
            $driverChain->addDriver($defaultDriver, $key);
        }

        $config->setMetadataDriverImpl($driverChain);

        if (self::$entityManager == null) {
            $conn = array(
                'driver'   => 'pdo_mysql',
                'user'     => DB_USER,
                'password' => DB_PASSWORD,
                'host' => DB_HOST,
                'dbname'   => DB_NAME,
                'charset' => 'utf8'
            );
        } else {
            $conn = self::$entityManager->getConnection();
        }

        if (self::$eventManager == null) {
            // create event manager and hook preferred extension listeners
            $evm = new EventManager();

            //timestampable
            $timestampableListener = new \Gedmo\Timestampable\TimestampableListener;
            $evm->addEventSubscriber($timestampableListener);

            //Softdeleteable
            $softDeleteableListener = new \Gedmo\SoftDeleteable\SoftDeleteableListener;
            $evm->addEventSubscriber($softDeleteableListener);

            self::$eventManager = $evm;
        }

        $config->addFilter('softDeleteable', '\Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter');

        self::$entityManager = EntityManager::create($conn, $config, self::$eventManager);
        self::$entityManager->getFilters()->enable('softDeleteable');

        return self::$entityManager;
    }

    public static function wordpressConfigFolder()
    {
        $currentDir = __DIR__;

        while ($currentDir != '/') {
            $files = scandir($currentDir);

            foreach ($files as $file) {
                if ($file == 'wp-config.php') {
                    return $currentDir;
                }
            }

            $currentDir = dirname($currentDir);
        }

        echo 'wp-config not found';
        die();
    }
}