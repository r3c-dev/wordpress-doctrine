## R3C Wordpress Doctrine


Pacote para integração do Doctrine com o Wordpress


# Mapeia a pasta das entidades -> Namespace, Diretório com as entidades #


```
#!php

DoctrineFactory::addEntityPath('Teste\Entities', __DIR__ . '/src/Teste/Entities');
```


# Após mapear, use o EntityManager a vontade ;-) #



```
#!php

$entityManager = DoctrineFactory::entityManager();
```

# Copiar o cli-config.php para a pasta Root do seu projeto #

Após copiar, você pode executar comandos como:


```
#!bash

vendor/bin/doctrine orm:schema-tool:update --force
```
